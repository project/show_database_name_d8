INTRODUCTION
------------

This module helps to display the host and database name of the default
database on the admin menu toolbar. This module is integrated 
from the D7 Show Database Name.

We can quickly identify which database that we are using when switching
from development to staging to production by using this module.

This module is integrated from the Drupal 7 Show Database Name
(https://www.drupal.org/project/show_database_name) was developed by
Gregg Marshall (https://www.drupal.org/u/greggmarshall).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/show_database_name_d8

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/show_database_name_d8


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.


MAINTAINERS
-----------

Current maintainers:
 * Kurinjiselvan V - https://www.drupal.org/u/kurinjiselvan-v